money = 500
phone_cost = 240
tablet_cost = 200

total_cost = phone_cost + tablet_cost
can_afford_both = money > total_cost
can_afford_phone = money > phone_cost

if can_afford_both:
    message = "You have enough money for both"
elif can_afford_phone:
    message = "You can  afford phone "

print(message)  # what do you expect to see here?
