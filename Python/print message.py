message = "Level Two"
#print(message)
print(type(message))

message = 123
#print(message)
print(type(message))

message = 'j'
#print(message)

message = 123.325
#print(message)

print("message is %0.2f" %message)

print("message is %d" %message)

print(type(message))