addresses = {
    'Lauren': '0161 5673 890',
    'Amy': '0115 8901 165',
    'Daniel': '0114 2290 542',
    'Emergency': '999'
}

### You access dictionary elements by looking them up with the key
print(addresses['Amy'])
##
### You can check if a key or value exists in a given dictionary:
##
print('David' in addresses)  # [False]
print('Daniel' in addresses)  # [True]
print('999' in addresses)  # [False]
print('999' in addresses.values())  # [True]
print(999 in addresses.values())  # [False]