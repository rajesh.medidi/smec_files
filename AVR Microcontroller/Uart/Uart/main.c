#include<avr/io.h>
#define F_CPU 8000000UL
#include<util/delay.h>

int main()
{
	UBRRL=0x33;
	UCSRB=0x18;
	UCSRC=0x86;

	UDR='L';
	while((UCSRA&0x40)==0);
	_delay_ms(100);
	UCSRA=0x40;
	while(1);
}
