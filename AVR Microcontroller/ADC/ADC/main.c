#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
void command(unsigned char);
void data(unsigned char);
void display(char *p);

int main(void){
	int a,b,x=1234,y=0,z,p,q,r,s,sw1;
	DDRA=0x00;
	DDRD=0xFF;
	DDRC=0x03;
	ADCSRA=0xD7;
	ADMUX=0x00;
	command(0x38);
	command(0x80);
	command(0x0f);
	command(0x06);
	command(0x01);
	while(1){
		ADCSRA=0xD7;
		ADCSRA=0x40;
		while(ADCSRA&0x40==0x40);
		a=ADCL;
		a|=(ADCH<<8);
		p=a/1000;
		data(p+48);
		_delay_ms(10);
		q=(a%1000);
		y=q;
		q=q/100;
		data(q+48);
		y=(y%100);
		s=y;
		y=y/10;
		data(y+48);
		r=(s%10);
		data(r+48);
		_delay_ms(10);
		command(0x01);
	
	}
}

void command(unsigned char a){
	PORTD=a;
	PORTC=0x02; //RS=0 and EN=1
	_delay_ms(10);
	PORTC=0x00;//RS=0 and EN=0
}

void data(unsigned char b){
	PORTD=b;
	PORTC=0x03; //RS=1
	_delay_ms(10);
	PORTC=0x01; //EN=0 and RS=1
}

void display(char *p)
{
	while (*p!='\0')
	{
		data(*p);
		p++;
	}
}