#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
void command(unsigned char);
void data(unsigned char);
void display(char *p);

int main(void)
{	
	DDRD=0x70;
	DDRB=0xFF;
	PORTD=0x50;
	char arr[]="HAVE A NICE DAY";
	char arr1[]="     :) ";
	command(0x38);
	command(0x80);
	command(0x0f);
	command(0x06);
	command(0x01);

	display(arr);
	command(0xC0);
	display(arr1);
	command(0x0C);

    while(1);
}

void command(unsigned char a){
	PORTB=a;
	PORTD=0x40; //RS=0 and EN=1
	_delay_ms(100);
	PORTD=0x00;//RS=0 and EN=0
}

void data(unsigned char b){
	PORTB=b;
	PORTD=0x50; //RS=1 AND en =1
	_delay_ms(100);
	PORTD=0x10; //EN=0 and RS=1
}

void display(char *p)
{
	while (*p!='\0')
	{
		data(*p);
		p++;
	}
}
