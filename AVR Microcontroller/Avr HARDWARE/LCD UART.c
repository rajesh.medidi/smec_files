#include<avr/io.h>
#define F_CPU 8000000UL
#include<util/delay.h>
void transmit(char);
char receive();

void command(unsigned char);
void data(unsigned char);
void display(char *p);

int main()
{	
	DDRD=0x72;// Uart and Lcd interface
	DDRB=0xFF;
	PORTD=0x50;//R/w=0,En&rs=1
	command(0x38);
	command(0x80);
	command(0x0f);
	command(0x06);
	command(0x01);

	char a;
	UBRRL=0x33;
	UBRRH=0x00;
	UCSRB=0x18;
	UCSRC=0x86;
	int i;
	
	while(1){
		a=receive();
		transmit(a);
		data(a);	
	}	
}
	
	
void transmit(char a){
	UDR=a;
	while((UCSRA&0x40)==0);
	UCSRA=0x40;
}

char receive(){
	while((UCSRA&0x80)==0);
	return(UDR);
}	



void command(unsigned char a){
	PORTB=a;
	PORTD=0x40; //RS=0 and EN=1
	_delay_ms(100);
	PORTD=0x00;//RS=0 and EN=0
}

void data(unsigned char b){
	PORTB=b;
	PORTD=0x50; //RS=1 AND en =1
	_delay_ms(100);
	PORTD=0x10; //EN=0 and RS=1
}

void display(char *p)
{
	while (*p!='\0')
	{
		data(*p);
		p++;
	}
}

