#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
void command(char);
void data(char);
void display(char *p);
char keypad();

int main(void)
{
	DDRB=0xF0;
	DDRD=0xFF;
	DDRC=0x0F;
	command(0x38);
	command(0x80);
	command(0x0f);
	command(0x06);
	command(0x01);
	char realpass[4]="1234",user[4];
	char arr1[]="Access Granted";
	char arr2[]="Access Denied";
	char x=0,y;
	 int b;
	while(1){
		
		for(int p=0;p<4;p++){
			user[p]=keypad();
			data(user[p]);
		}
		for (int p=0;p<4;p++){
			if(user[p]==realpass[p])
			b++;
		}
		if (b==4){
			command(0x01);
			display(arr1);
		}
		else{
			command(0x01);
			display(arr2);
		}
	}	
}

char keypad(){
	while(1){
		PORTB=0x7F;
		int a;
		a=PINB&0x0f;
		switch(a){
			case 0x07:
			return('1');
			case 0x0b:
			return('2');
			case 0x0d:
			return('3');
			case 0x0e:
			return('4');

		}
		
		PORTB=0xBF;
		a=PINB&0x0f;
		switch(a){
			case 0x07:
			return('5');
			case 0x0b:
			return('6');
			case 0x0d:
			return('7');
			case 0x0e:
			return('8');
		}
		
		PORTB=0xDF;
		a=PINB&0x0f;
		switch(a){
			case 0x07:
			return('9');
			case 0x0b:
			return('0');
			case 0x0d:
			return('A');
			case 0x0e:
			return('B');
		}
		
		PORTB=0xEF;
		a=PINB&0x0f;
		switch(a){
			case 0x07:
			return('C');
			case 0x0b:
			return('D');
			case 0x0d:
			return('E');
			case 0x0e:
			return('F');
		}
	}
	
}

void command(char a){
	PORTD=a;
	PORTC=0x02; //RS=0 and EN=1
	_delay_ms(10);
	PORTC=0x00;//RS=0 and EN=0
}

void data(char b){
	PORTD=b;
	PORTC=0x03; //RS=1
	_delay_ms(10);
	PORTC=0x01; //EN=0 and RS=1
	_delay_ms(10);
	 while((PINB&0x0f)!=0x0f);
}

void display(char *p)
{
	while (*p!='\0')
	{
		data(*p);
		p++;
	}
}

