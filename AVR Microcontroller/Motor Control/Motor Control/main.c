#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#define EN PC1
#define RS PC0
void command(unsigned char);
void data(unsigned char);
void display(char *p);

int main(void)
{
	DDRD=0xFF;
	DDRC=0x03;
	DDRB=0x00;
	DDRA=0xFF;
	int a=0,b=0;
	char arr[]="Anticlockwise";
	char arr1[]="Clockwise     ";
	char arr2[]="Stop          ";
	command(0x38);
	command(0x80);
	command(0x0f);
	command(0x06);
	command(0x01);
	while(1)
	{
		if((PINB&0x01)==0){
			a++;
			while((PINB&0x01)==0);


			if(a%3==0){
			//	_delay_ms(100);
				PORTA|=0x06;
				PORTA&=~0x01;
				command(0x80);
				display(arr1);
			}
			else if(a%3==1){
				PORTA|=0x05;
				PORTA&=~0x02;
				command(0x80);
				display(arr);
			}
			else {
			//	_delay_ms(100);
				PORTA|=0x00;
				PORTA&=~0x07;
				command(0x80);
				display(arr2);
			}
		}
		if((PINB&0x02)==0){
			b++;
			while((PINB&0x02)==0);
			if(b%3==0){
				command(0xc0);
				PORTA|=0x28;
				PORTA&=~0x10;
				display(arr1);
			}
			else if(b%3==1){
				command(0xc0);
				PORTA|=0x18;
				PORTA&=~0x20;
				display(arr);
			}
			else{
				command(0xc0);
				PORTA|=0x00;
				PORTA&=~0x38;
				display(arr2);
			}

		}
	}
	}

void command(unsigned char a){
	PORTD=a;
	PORTC=0x02; //RS=0 and EN=1
	_delay_ms(3);
	PORTC=0x00;//RS=0 and EN=0
}

void data(unsigned char b){
	PORTD=b;
	PORTC=0x03; //RS=1
	_delay_ms(3);
	PORTC=0x01; //EN=0 and RS=1
}

void display(char *p)
{
	while (*p!='\0')
	{
		data(*p);
		p++;
	}
}
