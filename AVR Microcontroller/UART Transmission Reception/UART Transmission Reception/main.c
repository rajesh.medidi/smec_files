#include<avr/io.h>
#define F_CPU 8000000UL
#include<util/delay.h>
void transmit(char);
char receive();

int main()
{	
	DDRD=0X02;
	char a;
	UBRRL=0x33;
	UBRRH=0x00;
	UCSRB=0x18;
	UCSRC=0x86;
	int i;
	
	while(1){
		a=receive();
		transmit(a);	
	}	
}
	
	
void transmit(char a){
	UDR=a;
	while((UCSRA&0x40)==0);
	UCSRA=0x40;
}

char receive(){
	while((UCSRA&0x80)==0);
	return(UDR);
}	
