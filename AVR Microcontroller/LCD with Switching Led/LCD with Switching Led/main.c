#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#define EN PC1
#define RS PC0
void command(unsigned char);
void data(unsigned char);
void display(char *p);

int main(void)
{
	DDRD=0xFF;
	DDRC=0x03;
	DDRA=0xFF;
	DDRB=0x00;
	char arr[]="BLINKING";
	char arr1[]="LEFT SHIFT";
	char arr2[]="RIGHT SHIFT";
	char arr3[]="Clearing Screen";
	command(0x38);
	command(0x80);
	command(0x0f);
	command(0x06);
	command(0x01);

	while(1){
		
			if((PINB&0x01)==0){
				command(0x01);
				display(arr);
				PORTA=0xFF;
				_delay_ms(100);
				PORTA=0x00;
				_delay_ms(800);
				command(0x01);
				//while((PINB&0x01)==0);
			}
			
			else if((PINB&0x02)==0){
				command(0x01);
				display(arr1);
				PORTA=0x80;
				_delay_ms(100);
				for(int i=0;i<=7;i++){
					PORTA=PORTA>>1;
					_delay_ms(100);
				//while((PINB&0x02)==0);
				}
				command(0x01);
			}
			
			else if((PINB&0x04)==0){
				command(0x01);
				display(arr2);
				PORTA=0x01;
				_delay_ms(100);
				for(int i=0;i<=7;i++){
					PORTA=PORTA<<1;
					_delay_ms(100);
				//	while((PINB&0x01)==0);
				}
				command(0x01);
			}
			else if((PINB&0x08)==0){
				command(0x01);
				display(arr3);
				_delay_ms(1000);
				command(0x01);
			//	while((PINB&0x01)==0);
			}
	}
}
void command(unsigned char a){
	PORTD=a;
	PORTC=0x02; //RS=0 and EN=1
	_delay_ms(100);
	PORTC=0x00;//RS=0 and EN=0
}

void data(unsigned char b){
	PORTD=b;
	PORTC=0x03; //RS=1
	_delay_ms(100);
	PORTC=0x01; //EN=0 and RS=1
}

void display(char *p)
{
	while (*p!='\0')
	{
		data(*p);
		p++;
	}
}