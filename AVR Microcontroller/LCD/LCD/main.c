#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#define EN PC1
#define RS PC0
void command(unsigned char);
void data(unsigned char);
//void display(char *p);

int main(void)
{
	DDRD=0xFF;
	DDRC=0x03;
	char arr[]="HAVE A NICE DAY";
	char arr1[]="     :) ";
	command(0x38);
	command(0x80);
	command(0x0f);
	command(0x06);
	command(0x01);
	display(arr);
	command(0xC0);
	display(arr1);
	command(0x0C);
    while(1);
}

void command(unsigned char a){
	PORTD=a;
	PORTC=0x02; //RS=0 and EN=1
	_delay_ms(100);
	PORTC=0x00;//RS=0 and EN=0
}

void data(unsigned char b){
	PORTD=b;
	PORTC=0x03; //RS=1
	_delay_ms(100);
	PORTC=0x01; //EN=0 and RS=1
}

void display(char *p)
{
	while (*p!='\0')
	{
		data(*p);
		p++;
	}
}