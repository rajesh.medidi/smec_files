#include<pic18.h>
#define RS RC0
#define EN RC1
void data(char);
void command(char );
void delay(int ms);
void display(char *p);

void main(){
    ADCON1=0x0F;
    TRISA=0xFF;
    PORTA=0x00;
    TRISD=0x00;
    TRISC=0x00;
    PORTD=0x00;
    command(0x38);
    command(0x80);
    command(0x0f);
    command(0x06);
    command(0x01);
    char arr[]="Blinking";  
    char arr1[]="Left Shift";
    char arr2[]="Right Shift";
    while(1){
    if (RA0==0){
        display(arr);
        delay(1000);
        while(RA0==0);
    }
    else if(RA1==0){
        display(arr1);
        delay(1000);
        while(RA1==0);
    }
    else if(RA2==0){
        display(arr2);  
        delay(1000);
        while(RA2==0);
    } 
    else if(RA3==0){
        command(0x01);
    }
    }
}
void display(char *p)
{
    while (*p!='\0')
    {
    data(*p);
    p++;
    }
}
void command(char a){
    PORTD=a;
    RC0=0;
    RC1=1;
    delay(1000);
    RC1=0;
    delay(1000);
}
void data(char b){
    PORTD=b;
    RS=1;
    EN=1;
    delay(1000);
    EN=0;
}
void delay(int ms){
    for(int i=0;i<=ms;i++){
        
    }
}