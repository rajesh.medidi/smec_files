#include<pic18.h>
void delay(int ms);
void data(char);
void command(char );
void display(char *p);
char arr[]="Interrupt received";
void main(){
    TRISA=0x00;
    TRISD=0x00;
    ADCON1=0x0f;
    GIEH=1;
    INT0IE=1;
    PEIE=1;
    TRISC=0x00;
    TRISD=0x00;
    command(0x38);
    command(0x80);
    command(0x0f);
    command(0x06);
    command(0x01);
    
    
    while(1){
        RA5=0x00;
        delay(100);
        RA5=0xff;
        delay(100);
    }
}

void __interrupt() ISR(void){
    if(INT0IF==1){
        display(arr);
        command(0x01);
        RA0=~RA0;
    }
    INT0IF=0;
}

void data(char b){
    PORTD=b;
    RC0=1;
    RC1=1;
    delay(1000);
    RC1=0;
    delay(1000);
}
void command(char c){
    PORTD=c;
    RC0=0;
    RC1=1;
    delay(1000);
    RC1=0;
    delay(1000);
}
void display(char *p)
{
    while (*p!='\0')
    {
    data(*p);
    p++;
    }
}
void delay(int ms){
    for (int i=0;i<=ms;i++){
        
    }
}