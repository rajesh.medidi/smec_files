#include<pic18.h>
void transmit(char);
char receive();
void display(char *p);
void delay(int ms);

void main(){
    char arr[]=" PWM with 25 per cent \r";
    char arr1[]=" PWM with 50 per cent \r";
    char arr2[]=" PWM with 75 per cent \r";
    TXSTA=0x24;
    RCSTA=0x90;
    SPBRG=129;
    TRISC7=1;
    while(1){
    char a=receive();

    if(a=='A'){
        transmit(a);
        display(arr);
        TRISC2=0;
        T2CON=0x7E;
        CCP1CON=0x2F;
        CCPR1L=0x3E;
    }
    if (a=='B'){
        transmit(a);
        display(arr1);
        TRISC2=0;
        T2CON=0x7E;
        CCP1CON=0x0F;
        CCPR1L=0x7D;
    }
    if(a=='C'){
        transmit(a);
        display(arr2);
        TRISC2=0;
        T2CON=0x7E;
        CCP1CON=0x2F;
        CCPR1L=0xBB;
    }
}
}
char receive(){
    while(RCIF==0);
    return RCREG;
}

void transmit(char a){
    TXREG=a;
    while(TXIF==0);
     TXIF=0;
}
void display(char *p){
    while (*p!='\0')
    {
    transmit(*p);
    p++;
    }
}
void delay(int ms){
    for(int i=0;i<=ms;i++){
        
    }
}