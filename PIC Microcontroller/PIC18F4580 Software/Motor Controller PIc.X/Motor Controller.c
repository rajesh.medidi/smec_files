#include<pic18.h>
#define RS RC0
#define EN RC1
void delay(int ms);
void data(char);
void command(char );
void display(char *p);
void main(){
    ADCON1=0x0F;
    TRISA=0x00;
    TRISB=0x00;
    RA0=0;
    RA1=1;
    RA2=1;
    RB0=1;
    RB1=1;
    RB2=0;
    char arr[]="AntiClockwise";
    char arr1[]="Clockwise";
    TRISD=0x00;
    TRISC=0x00;
    PORTD=0x00;
    command(0x38);
    command(0x80);
    command(0x0f);
    command(0x06);
    command(0x01);
    display(arr);
    command(0xC0);
    display(arr1);
    while(1);   
}
void display(char *p)
{
    while (*p!='\0')
    {
    data(*p);
    p++;
    }
}
void command(char a){
    PORTD=a;
    RC0=0;
    RC1=1;
    delay(1000);
    RC1=0;
    delay(1000);
}
void data(char b){
    PORTD=b;
    RS=1;
    EN=1;
    delay(1000);
    EN=0;
}
void delay(int ms){
    for(int i=0;i<=ms;i++){
        
    }
}
