#include<pic18.h>
void transmit(char a);
void i2c_init()
{
SSPSTAT=0x00;//1000
SSPCON1=0x28;//
SSPCON2=0x00;
SSPADD=49;//100khz=
}
void i2c_start()
{
SEN=1;
while(SEN==1);
}
void i2c_stop()
{
PEN=1;
while(PEN==1);//
}
void i2c_data(char a)
{
SSPIF=0;
SSPBUF=a;
while(SSPIF==0);
}
char i2c_read(char a)
{
RCEN=1;//receive 
while(RCEN);//
ACKDT=a;//1- NACK before stopping 0->ack
ACKEN=1;
while(ACKEN);
return SSPBUF;
}
void main()
{
	int i;
    //UART
    TXSTA=0x24;
    RCSTA=0x90;
    SPBRG=129;
    


    
TRISB=TRISD=0x00;
TRISC=0x18;
i2c_init();


while(1) 	
{
i2c_start();//start slave ad+R/w  
i2c_data(0x11);//0001 0000  W-> 0 R ->1 Write operation Executed (SLAVE ADDRESS AND R/W Operation)
transmit(i2c_read(0));
transmit(i2c_read(1));
i2c_stop();
for(i=0;i<=10000;i++);
}
}

void transmit(char a){
    TXREG=a;
    while(TXIF==0);
     TXIF=0;
}