#include<stdio.h>
#include<string.h>
struct books{
    char title[50];
    char author[50];
    char subject[50];
    int book_id;
};
void printbook(struct books *book);
int main(){
    struct books book1;
    struct books book2;
    struct books book3;
    
    /*Book 1 specification*/
    strcpy (book1.title,"Ikigai");
    strcpy (book1.author,"Japanese author");
    strcpy (book1.subject,"Non Fiction");
    book1.book_id=31221;
    
    /*Book2 specification*/
    strcpy (book2.title,"A Monk Who sold his Ferrari");
    strcpy (book2.author,"Someone");
    strcpy (book2.subject,"Non Fiction");
    book2.book_id=289974;
    
    /*Book3 specification*/
    strcpy (book3.title,"Rich Dad Poor Dad");
    strcpy (book3.author,"Someone");
    strcpy (book3.subject,"Non Fiction");
    book3.book_id=937733;
    
    printbook(&book1);
    printbook(&book2);
    printbook(&book3);
    return 0;
    
}
void printbook(struct books *book){
    printf("Book Title: %s\n",book->title); 
    printf("Book Author: %s\n",book->author);
    printf("Book Subject: %s\n",book->subject);
    printf("Book ID: %d\n\n",book->book_id);
}

