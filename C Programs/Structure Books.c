#include<stdio.h>
struct books{
    char bookname[50];
    char booktype[50];
    char bookauthor[50];
    int book_id;
};
int main(){
    struct books book1={"Oliver Twist","Fiction","oliver",01};
    struct books book2={"F U Money","Non-Fiction","Dan Lok",02};
    struct books book3={"IKIGAI","Non-Fiction","Hector Gracia",03};
    printf("Book Name: %s\nBook Type: %s\nBook Author: %s\nBook ID:%d\n\n",book1.bookname,book1.booktype,book1.bookauthor,book1.book_id);
    printf("Book Name: %s\nBook Type: %s\nBook Author: %s\nBook ID:%d\n\n",book2.bookname,book2.booktype,book2.bookauthor,book2.book_id);
    printf("Book Name: %s\nBook Type: %s\nBook Author: %s\nBook ID:%d\n\n",book3.bookname,book3.booktype,book3.bookauthor,book3.book_id);
    return 0;
    
}

