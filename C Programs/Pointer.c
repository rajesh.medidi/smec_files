#include <stdio.h>

int main () {

   int var = 50; 
   int  *ip;

   ip = &var;  

   printf("Address of var variable: %x\n", &var  );


   printf("Address stored in ip: %x\n", ip );
   printf("Value of *ip variable: %d\n", *ip );

   return 0;
}
