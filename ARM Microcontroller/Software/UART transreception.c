#include<lpc21xx.h>
char reception();
void transmit(char);

void main(){
 char a;
 IODIR0=0x00000001;
 PINSEL0=0x00000005;
 U0LCR=0X83;
 U0DLL=0X61;
 U0DLM=0X00;
 U0LCR=0X03;

while(1){
		a=reception();
		transmit(a);	
	}	
}

void transmit(char a){
U0THR=a;
while((U0LSR&0X20)==0);
}

char reception(){
while((U0LSR&0x01)==0);
return (U0RBR);
}

