/* This code works with SHARP IR Analog procimity sensor
 * It measures the distance and displays it on the serial monitor every 2s
 * Refer to www.surtrtech.com for more information
 */

#include <SharpIR.h>    //IR sensor library

#define ir A0           //IR sensor analog output pin
#define model 20150  //or1080 depends on the model it actually means the range in cm 20-150 or 10-80
                     //  1080 for GP2Y0A21Y
                    //  20150 for GP2Y0A02Y

String Data;        //To store the sentence to display

SharpIR IR_prox(ir,model);  //define the sensor

void setup() {
  Serial.begin(9600);

}

void loop() {

  int dis_cm=IR_prox.getdistance(); //Read the distance in cm and store it

  Data = "Distance = "+String(dis_cm)+(" cm"); //group what to display

  Serial.println(Data);  //display the string e.g "Distance = 40 cm"
  delay(2000);           //done every 2 seconds, you can change it but make it at least 50 ms

}
