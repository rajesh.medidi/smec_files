#include <Servo.h>]
#include <LiquidCrystal.h>

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

Servo servo1;
int trigPin = 9;
int echoPin = 8;
long distance;
long duration;
 
void setup() 
{
servo1.attach(7); 
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT);// put your setup code here, to run once:
  lcd.begin(16, 2);
  // Print a message to the LCD.
//  lcd.print("  hello, world!");
}
 
void loop() {
  lcd.setCursor(0, 0);
  ultra();
  servo1.write(0);
  lcd.clear();
  if(distance <= 30){
  servo1.write(90);
lcd.print(" OPEN ");
    delay(1000);  
    
   
  }
  else if (distance > 30){
      lcd.print(" CLOSE");
      delay(1000);
    
 
  }
}
 
void ultra(){
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration*0.034/2;
  }
